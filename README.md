#Tecnologias utilizadas:
Se utilizo python 3.6
Html5
CSS3
Docker compose

#Entorno de produccion
https://cloud.google.com/appengine/docs/standard/python/getting-started/python-standard-env
gcloud app deploy

#Tabla base de datos
CREATE TABLE `contact` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`company`	TEXT NOT NULL,
	`phone`	TEXT NOT NULL,
	`mail`	TEXT NOT NULL,
	`adress`	TEXT NOT NULL,
	`city`	TEXT,
	`country`	TEXT,
	`website`	TEXT,
	`industry`	TEXT,
	`description`	TEXT,
	`transportemedio`	TEXT,
	`transportequimicos`	TEXT,
	`managername`	TEXT,
	`manageremail`	TEXT,
	`managerphone`	TEXT,
	`calidadname`	TEXT,
	`calidademail`	TEXT,
	`calidadphone`	TEXT,
	`finanzasname`	TEXT,
	`finanzasemail`	TEXT,
	`finanzasphone`	TEXT,
	`salename`	TEXT,
	`saleemail`	TEXT,
	`salephone`	TEXT,
	`cuentaname`	TEXT,
	`cuentaswift`	TEXT,
	`cuentaaba`	TEXT,
	`cuentanumber`	TEXT,
	`cuentabanco`	TEXT,
	`cuentaaddress`	TEXT,
	`certificados`	TEXT,
	`certificadoscomentarios`	TEXT,
	`certificadocumplimiento`	TEXT,
	`tiemporegistros`	TEXT,
	`trazabilidad`	TEXT,
	`status`	TEXT DEFAULT "new",
	`attach`	BLOB,
	`attach2`	BLOB,
	`attach3`	BLOB,
	`datevencimiento`	TEXT,
	`commecompras`	TEXT,
	`commeconta`	TEXT,
	`commecalidad`	TEXT
);

Masterlist-end

#Comandos para ejecutarlo 
$ export FLASK_APP=main.py
$ flask run
 * Running on http://127.0.0.1:5000/

If you are on Windows, the environment variable syntax depends on command line interpreter. On Command Prompt:

C:\path\to\app>set FLASK_APP=hello.py

And on PowerShell:

PS C:\path\to\app> $env:FLASK_APP = "hello.py"

Alternatively you can use python -m flask:

$ export FLASK_APP=hello.py
$ python -m flask run
 * Running on http://127.0.0.1:5000/

#Comando para instalar dependencias
pip install -r requirements.txt


#Activar modo envirinment
En Linux/Mac:

    export FLASK_ENV="development"

En Windows:

    set "FLASK_ENV=development"

#Logger para registrar los errores de la app
app.logger.debug('Mensaje de debug')
app.logger.warning('Una advertencia (%d advertencias)', 55)
app.logger.error('Un error fatal ha ocurrido!')



#Link google console
https://console.cloud.google.com/cloudshell/editor?project=masterlist-####&authuser=5&folder&organizationId
